/**************************************/
/*              ARE 2008              */
/*      e-mail: biuro@are.net.pl      */
/*      www   : are.net.pl            */
/**************************************/

#define F_CPU 8000000UL  // 1 MHz
//#define F_CPU 14.7456E6
#include <avr/io.h>
#include <stdio.h>
#include <util/delay.h>
#include <string.h>

void delay_ms(int ms)
	{
	volatile long unsigned int i;
	for(i=0;i<ms;i++)
		_delay_ms(1);
	}
	
void delay_us(int us)
	{
	volatile long unsigned int i;
	for(i=0;i<us;i++)
		_delay_us(1);
	}

//RS PA0
//RW PA1
//E  PA2
//DATA PD

#define RS 0
#define RW 1
#define E  2

#define _CV1_PIN PC3
#define _CV2_PIN PC4
#define SW_PIN PINB
#define SW1 PB0
#define SW5 PB1
#define SW9 PB2
#define SW13 PB3
#define CHECK_SW1 (!(SW_PIN & (1 << SW1)))
#define CHECK_SW5 (!(SW_PIN & (1 << SW5)))
#define CHECK_SW9 (!(SW_PIN & (1 << SW9)))
#define CHECK_SW13 (!(SW_PIN & (1 << SW13)))

#define SET_CV1 (PORTC &= ~(1 << _CV1_PIN))
#define RESET_CV1 (PORTC |= (1 << _CV1_PIN))
#define SET_CV2 (PORTC &= ~(1 << _CV2_PIN))
#define RESET_CV2 (PORTC |= (1 << _CV2_PIN))


void LCD2x16_init(void)
{
PORTC &= ~(1<<RS);
PORTC &= ~(1<<RW);

PORTC |= (1<<E);
PORTD = 0x38;   // dwie linie, 5x7 punktow
PORTC &=~(1<<E);
_delay_us(120);

PORTC |= (1<<E);
PORTD = 0x0e;   // wlacz wyswietlacz, kursor, miganie
PORTC &=~(1<<E);
_delay_us(120);

PORTC |= (1<<E);
PORTD = 0x06;
PORTC &=~(1<<E);
_delay_us(120);
}

void LCD2x16_clear(void){
PORTC &= ~(1<<RS);
PORTC &= ~(1<<RW);

PORTC |= (1<<E);
PORTD = 0x01;
PORTC &=~(1<<E);
delay_ms(120);
}

void LCD2x16_putchar(int data)
{
PORTC |= (1<<RS);
PORTC &= ~(1<<RW);

PORTC |= (1<<E);
PORTD = data;
PORTC &=~(1<<E);
_delay_us(120);
}

void LCD2x16_pos(int wiersz, int kolumna)
{
PORTC &= ~(1<<RS);
PORTC &= ~(1<<RW);

PORTC |= (1<<E);
delay_ms(1);
PORTD = 0x80+(wiersz-1)*0x40+(kolumna-1);
delay_ms(1);
PORTC &=~(1<<E);
_delay_us(120);
}



int main(void){
char www[16] = "www-> are.net.pl";
char email[16] = "biuro@are.net.pl";
char tmp[32];

int i;
int j=4;
int _sp = 600;
int _h = 80;
int _n = 2 * _h;
int _cv1 = 1;
int _cv2 = 1;
int _pv = 0;
int _e = _sp - _pv;

DDRA = 0x00;
DDRD = 0xff;
PORTD = 0x00;
DDRC = 0xff;
RESET_CV1;
RESET_CV2;
DDRB = 0x00;
PORTB = 0x0f;

ADMUX=0x40;
ADCSRA=0xe0;

_delay_ms(200);

LCD2x16_init();
LCD2x16_clear();

/*
    for(i=0;i < 16;i++)
		LCD2x16_putchar(www[i]);
	LCD2x16_pos(2,1);
	for(i=0;i < 16;i++)
		LCD2x16_putchar(email[i]);

delay_ms(3000);*/
LCD2x16_clear();

while(1)
	{ //Kowaczek Pacyniak
	if (CHECK_SW1)  {
		_sp = 500;
	}
	else if (CHECK_SW5) {
		_sp = 400;
	}
	else if (CHECK_SW9) {
		_h = 40;
		_n = 2 * _h;
	}
	else if (CHECK_SW13) {
		_h = 100;
		_n = 2 * _h;
	}
		
	_pv = ((long) ADC * 1000) / 1023;
	_e = _sp - _pv;
	
	// CV1
	if(_e > _n / 2 + _h) {
		SET_CV1;
	}
	if(_e < _n / 2) {
		RESET_CV1;
	}
	
	// CV2
	if(_e < -_n / 2 - _h) {
		SET_CV2;
	}
	if(_e > -_n / 2) {
		RESET_CV2;
	}
	
	//LCD2x16_clear();
	LCD2x16_pos(1,1);
	sprintf(tmp,"SP=%02d%% PV=%03d.%d%%", _sp / 10, _pv / 10, _pv % 10);
	for(i=0;i < 16;i++)
		LCD2x16_putchar(tmp[i]);
	
	LCD2x16_pos(2,1);
	sprintf(tmp,"H=%02d%%  E=%+04d.%d%%", _h / 10, _e / 10, _e < 0 ? -_e % 10 : _e % 10);
	for(i=0;i < 16;i++)
		LCD2x16_putchar(tmp[i]);
	delay_ms(100);
	}

return 0;
}