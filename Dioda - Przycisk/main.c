#include <avr/io.h>
#include <util/delay.h>

#define INIT_LED_PA0 DDRA |= (1<<PA0) //output PA0
#define LED_ON 		 PORTA |=(1<<PA0)
#define LED_OFF 	 PORTA &= ~(1<<PA0)

#define INIT_SW 	 DDRA &= ~(1<<PA2)
#define PULLUP_SW 	 PORTA |= (1<<PA2)


int main (void)
{
	INIT_LED_PA0;
	INIT_SW;
	PULLUP_SW;
	
	while(1)
	{
		if(!(PINA & (1<<PA2)))
			LED_ON;
		else
			LED_OFF;
	}
	return 0;
}