#include<avr/io.h>

#define ZERO 0x3F
#define ONE 0x06
#define TWO 0x5B
#define THREE 0x4F
#define FOUR 0x66
#define FIVE 0x6D
#define SIX 0x7D
#define SEVEN 0x07
#define EIGHT 0x7F
#define NINE 0x6F


#define INIT_SWD 	 DDRD &= ~(1<<PD7)
#define PULLUP_SWD 	 PORTD |= (1<<PD7)

#define INIT_SWC 	 DDRC &= ~(1<<PC3)
#define PULLUP_SWC 	 PORTC |= (1<<PC3)

void init_7_seg()
{
  DDRA = 0x7F;
  PORTA = 0X00;
}

void display_number(int no)
{
  switch(no)
  {
	case 0:
		PORTA = ZERO;
		break;
	case 1:
		PORTA = ONE;
		break;
	case 2:
		PORTA = TWO;
		break;
	case 3:
		PORTA = THREE;
		break;
	case 4:
		PORTA = FOUR;
		break;
	case 5:
		PORTA = FIVE;
		break;
	case 6:
		PORTA = SIX;
		break;
	case 7:
		PORTA = SEVEN;
		break;
	case 8:
		PORTA = EIGHT;
		break;
	case 9:
		PORTA = NINE;
		break;
	}
}

int main()
{
	INIT_SWD;
	PULLUP_SWD;
	INIT_SWC;
	PULLUP_SWC;
	init_7_seg();
	// initcjalizacja bramek i port�w
	
	int new_number = 0;
	
	while(1)
	{
		display_number(new_number);
		
		if(!(PIND & (1<<PD7)))
		{
			while(!(PIND & (1<<PD7)));
			
			new_number++;
			
			if(new_number == 10)
			{
				new_number = 9;
			}
			
		}
		
		// inkrementacja segmentarza
		
		if(!(PINC & (1<<PC3)))
		{
			while(!(PINC & (1<<PC3)));
			
			new_number--;
			
			if(new_number == -1)
			{
				new_number= 0;
			}
			
		}
		
		// dekrementacja segmentarza

	}
}
