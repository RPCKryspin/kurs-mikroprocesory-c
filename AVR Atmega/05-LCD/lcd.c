/**************************************/
/*              ARE 2008              */
/*      e-mail: biuro@are.net.pl      */
/*      www   : are.net.pl            */
/**************************************/

#define F_CPU 8000000UL  // 1 MHz
//#define F_CPU 14.7456E6
#include <avr/io.h>
#include <stdio.h>
#include <util/delay.h>
#include <string.h>

void delay_ms(int ms)
	{
	volatile long unsigned int i;
	for(i=0;i<ms;i++)
		_delay_ms(1);
	}
	
void delay_us(int us)
	{
	volatile long unsigned int i;
	for(i=0;i<us;i++)
		_delay_us(1);
	}

//RS PA0
//RW PA1
//E  PA2
//DATA PD

#define RS 0
#define RW 1
#define E  2

void LCD2x16_init(void)
{
PORTB &= ~(1<<RS);
PORTB &= ~(1<<RW);

PORTB |= (1<<E);
PORTD = 0x38;   // dwie linie, 5x7 punktow
PORTB &=~(1<<E);
_delay_us(120);

PORTB |= (1<<E);
PORTD = 0x0e;   // wlacz wyswietlacz, kursor, miganie
PORTB &=~(1<<E);
_delay_us(120);

PORTB |= (1<<E);
PORTD = 0x06;
PORTB &=~(1<<E);
_delay_us(120);
}

void LCD2x16_clear(void){
PORTB &= ~(1<<RS);
PORTB &= ~(1<<RW);

PORTB |= (1<<E);
PORTD = 0x01;
PORTB &=~(1<<E);
delay_ms(120);
}

void LCD2x16_putchar(int data)
{
PORTB |= (1<<RS);
PORTB &= ~(1<<RW);

PORTB |= (1<<E);
PORTD = data;
PORTB &=~(1<<E);
_delay_us(120);
}

void LCD2x16_pos(int wiersz, int kolumna)
{
PORTB &= ~(1<<RS);
PORTB &= ~(1<<RW);

PORTB |= (1<<E);
delay_ms(1);
PORTD = 0x80+(wiersz-1)*0x40+(kolumna-1);
delay_ms(1);
PORTB &=~(1<<E);
_delay_us(120);
}



int main(void){
char www[16] =   "TLK Artus       ";
char email[16] = " Rozklad jazdy  ";
char stacje1[16]="Wroclaw Glowny -";
char stacje2[16]="Leszno   -      ";
char stacje3[16]="Poznan Glowny - ";
char stacje4[16]="Gdansk Glowny   ";

char tmp[16];
char LCD[16] = "Pomiar";

ADMUX=0x40;
ADCSRA=0xe0;

int i;
int j=4;
int pomiar1;

DDRD = 0xff;
PORTD = 0x00;
DDRB = 0xff;
PORTB = 0x00;

DDRA=0x00;
//PORTA=0xff;

_delay_ms(200);

LCD2x16_init();
LCD2x16_clear();


    for(i=0;i < 16;i++)
		LCD2x16_putchar(www[i]);
	LCD2x16_pos(2,1);
	for(i=0;i < 16;i++)
		LCD2x16_putchar(email[i]);

delay_ms(1000);
LCD2x16_clear();

for(i=0;i < 16;i++)
		LCD2x16_putchar(stacje1[i]);
	LCD2x16_pos(2,1);
	for(i=0;i < 16;i++)
		LCD2x16_putchar(stacje2[i]);

delay_ms(1000);
LCD2x16_clear();

for(i=0;i < 16;i++)
		LCD2x16_putchar(stacje3[i]);
	LCD2x16_pos(2,1);
	for(i=0;i < 16;i++)
		LCD2x16_putchar(stacje4[i]);

delay_ms(1000);
LCD2x16_clear();

for(i=0;i < 16;i++)
	LCD2x16_putchar(www[i]);

while(1)
	{
	LCD2x16_clear();
	LCD2x16_pos(2,1);
	sprintf(tmp,"U_DC:   %2i V   ",pomiar1);
	pomiar1 = ADC;
	for(i=0;i < 16;i++)
		LCD2x16_putchar(tmp[i]);
	delay_ms(250);   
	}

return 0;
}