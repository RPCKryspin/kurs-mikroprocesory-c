#define F_CPU 8000000L // predkosc micropc

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define INIT_LED_PC0 DDRC |= (1<<PC0)  
#define LED_ON 	PORTC |= (1<<PC0)
#define LED_OFF PORTC &= ~(1<<PC0)

int counter= 125;

ISR(TIMER0_OVF_vect)
{
	
	TCNT0 = 5;
	if(!counter--)
	{
		if(PINC & (1<<PC0))
			LED_OFF;
		else
			LED_ON;
		counter= 125;
	}	
}


int main(void)
{
	INIT_LED_PC0;
	sei();
	
	TCCR0 |= (1<<CS02);	//preskaler na 256
	TCNT0 = 5;				//warto�� pocz�tkowa licznika na 5 (liczymy o 5 do 255)
	TIMSK |= (1<<TOIE0);	//w��czenie przerwa�	
	
	while(1);
}
