#define F_CPU 16000000L // predkosc micropc

#include<avr/io.h>
#include<util/delay.h>

#define ZERO 0x3F
#define ONE 0x06
#define TWO 0x5B
#define THREE 0x4F
#define FOUR 0x66
#define FIVE 0x6D
#define SIX 0x7D
#define SEVEN 0x07
#define EIGHT 0x7F
#define NINE 0x6F

void init_7_seg()
{
  DDRD = 0x7F;
  PORTD = 0X00;
}
void clear_display()
{
	PORTD = 0X00;
}

void display_number(int no)
{
  switch(no)
  {
	case 0:
		PORTD = ZERO;
		break;
	case 1:
		PORTD = ONE;
		break;
	case 2:
		PORTD = TWO;
		break;
	case 3:
		PORTD = THREE;
		break;
	case 4:
		PORTD = FOUR;
		break;
	case 5:
		PORTD = FIVE;
		break;
	case 6:
		PORTD = SIX;
		break;
	case 7:
		PORTD = SEVEN;
		break;
	case 8:
		PORTD = EIGHT;
		break;
	case 9:
		PORTD = NINE;
		break;
	}
}



int Pomiar(void)
{
	ADCSRA |= (1<<ADSC); // START KONWERSJI
	while(ADCSRA & (1<<ADSC));
	return ADC;
}



int main()
{
	int a,b,c,d;
	
	ADMUX |= (1<< REFS0); //ustawienie napiecia ref na napiecie zasilania
	ADMUX |= ((1<< MUX0) | (1<< MUX1) | (1<< MUX2)); //WYBOR KANA�U ADC -> ADC7

	ADCSRA |= (1<<ADEN); //URUCHOMIENIE PRZETWORNIKA ADC
	ADCSRA |= ((1<<ADPS0) | (1<<ADPS1)); // USTAWIENIE PRESKALERA NA 8 

	DDRA |= 1 << PA0;
	DDRC |= 0x0F;
	
	init_7_seg();

	while(1)
	{
		int zm = Pomiar(); 
		if(zm < 512)
		{
			PORTA |= 1 << PA0;
		}
		else
		{
			PORTA &= ~(1 << PA0);
		}
		a = zm/1000; //3
		b = (zm-a*1000)/100; //2
		c = (zm-a*1000 - b*100)/10; //0
		d = zm % 10; //1
		
		PORTC |= 1<<PC3;
		display_number(a);
		_delay_ms(16);
		clear_display();
		PORTC &= ~(1<<PC3);
		
		PORTC |= 1<<PC2;
		display_number(b);
		_delay_ms(16);
		clear_display();
		PORTC &= ~(1<<PC2);
		
		PORTC |= 1<<PC1;
		display_number(c);
		_delay_ms(16);
		clear_display();
		PORTC &= ~(1<<PC1);
		
		PORTC |= 1<<PC0;
		display_number(d);
		_delay_ms(16);
		clear_display();
		PORTC &= ~(1<<PC0);
	}
	return 0;
	
	
}

