#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define ZERO 0x3F
#define ONE 0x06
#define TWO 0x5B
#define THREE 0x4F
#define FOUR 0x66
#define FIVE 0x6D
#define SIX 0x7D
#define SEVEN 0x07
#define EIGHT 0x7F
#define NINE 0x6F

int a = 5;

void init_7_seg()
{
  DDRA = 0x7F;
  PORTA = 0X00;
}

void display_number(int no)
{
  switch(no)
  {
	case 0:
		PORTA = ZERO;
		break;
	case 1:
		PORTA = ONE;
		break;
	case 2:
		PORTA = TWO;
		break;
	case 3:
		PORTA = THREE;
		break;
	case 4:
		PORTA = FOUR;
		break;
	case 5:
		PORTA = FIVE;
		break;
	case 6:
		PORTA = SIX;
		break;
	case 7:
		PORTA = SEVEN;
		break;
	case 8:
		PORTA = EIGHT;
		break;
	case 9:
		PORTA = NINE;
		break;
	}
}

ISR(INT0_vect)

{
	if(a==0)
	{
		a=a;
	}
	else
	{
		a--;
	}
}

ISR(INT1_vect)

{
	if(a==9)
	{
		a=a;
	}
	else
	{
		a++;
	}
}

int main()
{
	init_7_seg();
	
	sei(); //SREG
	
	MCUCR |= (1<<ISC01); //zbocze opadaj�ce - wyzwolenie przerwania
	GICR |= (1<<INT0); //aktywacja przerwania
	
	MCUCR |= (1<<ISC11); //zbocze opadaj�ce - wyzwolenie przerwania
	GICR |= (1<<INT1); //aktywacja przerwania
	
	while(1)
	{
		display_number(a);
	}
	return 0;
}